module.exports = {
    display: (lines) => {
        console.reset();
        lines.filter( line => {
            return line.a
        //}).map( line => {
            //return line.a
        }).map( line => {
            let a = line.a
            let logLine = ""
            for (let i = 0; i < a.depth; i++) {
                logLine += "\t"
            }
            logLine += `${line.tid}: ${a.type} ${a.name}`
            if (a.res) {
                logLine += ` ${JSON.stringify(a.res.ucpu)} ${a.elapsed}`
            }

            console.log(logLine)
        })
    }
}
