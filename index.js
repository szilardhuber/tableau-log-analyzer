
var lineReader = require('readline').createInterface({
  //input: require('fs').createReadStream('vizqlserver.log')
  //input: require('fs').createReadStream('hyper_2018_06_07_16_39_48.log')
  input: require('fs').createReadStream('hyper_2018_06_22_16_05_44 (1).log')
});

const pager = require('./pager')
const depth = require('./depth')
const sessions = require('./sessions')

let lines = []
let mode = ""

lineReader.on('close', () => {
    console.log("Lines: ", lines.length);
    pager.setData(lines)
})

lineReader.on('line', function (line) {
    const logLine = JSON.parse(line);
    //if (logLine.a) {
    //if (logLine.v && logLine.v['query-trunc']) {
    if (logLine.v && logLine.v['client-session-id'] == 'CDC0689A3FD3416499F04778E244F08F-0:0') {
        lines.push(logLine);
    }
});

var stdin = process.stdin;
stdin.setRawMode(true);
stdin.resume();
stdin.setEncoding('utf8');

console.reset = function () {
  return process.stdout.write('\033c');
}


console.log("Select mode");
console.log("\tP - Pager");
console.log("\tD - Depth CPU view");
stdin.on('data', function(key){
    if (mode == 'pager') {
        if (key == '\u001B\u005B\u0041') {
            pager.left()
            //process.stdout.write('up'); 
        }
        if (key == '\u001B\u005B\u0043') {
            pager.right()
            //process.stdout.write('right'); 
        }
        if (key == '\u001B\u005B\u0042') {
            pager.right()
            //process.stdout.write('down'); 
        }
        if (key == '\u001B\u005B\u0044') {
            pager.left();
            //process.stdout.write('left'); 
        }
    } else {
        if (key == 'p') 
        {
            mode = 'pager';
            pager.left()
        }
        if (key == 'd')
        {
            mode = 'session'
            sessions.display(lines)
        }
        if (key == '\r') {
            sessions.display(lines)
            //depth.display(lines)
            //process.exit()
        }
    }

    if (key == '\u0003') { process.exit(); }    // ctrl-c
});

