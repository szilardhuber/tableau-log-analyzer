let sessions = {}

module.exports = {
    display: (lines) => {
        sessions = {}
        lines.map( line => {
            if (!sessions[line.sess]) {
                sessions[line.sess] = {
                    ucpu: 0,
                    kcpu: 0,
                    from: new Date("3001-01-01T00:00:00.000"),
                    to: new Date("1001-01-01T00:00:00.000"),
                }
            }
            let ts = new Date(line.ts)
            if (sessions[line.sess].from > ts) {
                sessions[line.sess].from = ts
            }
            if (sessions[line.sess].to < ts) {
                sessions[line.sess].to = ts
            }
            if (line.a.depth == 0 && line.a.res) {
                sessions[line.sess].ucpu += line.a.res.ucpu.i;
                sessions[line.sess].kcpu += line.a.res.kcpu.i;
            }
        })
        console.reset();
        Object.keys(sessions).map( sess => {
            console.log(`\t${sess} - Length: ${(sessions[sess].to - sessions[sess].from) / 1000}s User CPU: ${sessions[sess].ucpu / 1000} Kernel CPU: ${sessions[sess].kcpu / 1000} `)
        })
    },
    up: () => {
    }
}
