let index = 0

let lines = []

const setData = (_lines) => {
    lines = _lines
}

const print = (index) => {
    if (index >= 0 && index < lines.length) {
        console.reset();
        console.dir(lines[index], { depth: 4 })
    }
}

const left = () => {
    if (index > 0) {
        index -= 1;
    }
    print(index)
}

const right = () => {
    if (index < lines.length - 1) {
        index += 1;
    }
    print(index)
}

module.exports = {
    setData,
    left,
    right
}
